<?php
//주석에 '수정'이라고 되어있는곳 모두 수정하기

include '../inc/common.php';
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.

// adop param
$ad_network='PANGLE';
$trans_id=$_GET['trans_id'];
$dev_id='';
$app_id=$_GET['app_id'];
$key_id= $_GET['key_id'];
$extraData = json_decode($_GET['extra'],true);
$zone_id=$extraData['zone_id'];
$user_id = $extraData['user_id'];
$eventId = $_GET['eventId'];
$amt = $_GET['rewards'];
$verifier = $_GET['sign'];
$timestamp = $_GET['timestamp'];

$zoneSecretList = array('980418653'=>'1705d7d6998ab43255c85df5f3b1aba5',
    '980418643'=>'181970c286720e1507775784f0c82f10',
    '980971997'=>'f069f31f70eda95cef40fdfa7ad14f7b',
    '980971984'=>'8593486ad1d068cc7251ad3d2eba7b53',
    '980971851'=>'7229b8667c3c5234cf62b2c0c73a4738',
    '980972023'=>'688d3fcf0a836b32a6aea6a585e2107d',
    '981021306'=>'7b68e784021e990466f33345a346377a');
$privateKey = $zoneSecretList[$zone_id];

$sign_string = $privateKey.":".$trans_id;
$in_file = "/Data/logs/".$ad_network.".log";

if (hash("sha256",$sign_string) != $verifier) {
    $in_data = "Signature doesn’t match parameters";
    file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);
    exit;
}

$in_data = date("Y-m-d.H:i:s")."WALKMINING[".$ad_network."] app_id=".$app_id.",zone_id=".$zone_id.",trans_id=".$trans_id.",dev_id=".$dev_id.",amt=".$amt.",verifier=".$verifier.",user_id=".$user_id.",time=".time().':'.$timestamp.",key_id=".$key_id;//수정

//앱마다 키 생성해주기(랜덤)
$WALKMINING_SECRET_KEY="24dbfb53-7715-4399-8647-d8a20521b878";//수정
//verify hash
$sign_result = $sign_string;

$get_data = array(
    'ad_network'=>$ad_network,
    'trans_id'=>$trans_id,
    'app_id'=>$app_id,
    'zone_id'=>$zone_id,
    'dev_id'=>$dev_id,
    'amt'=>$amt,
    'verifier'=>$sign_result,
    'user_id'=>$user_id
);

$result_data = getUA("https://runner-api.walkmining.com/v1/callback/adop", $get_data);//수정
$in_data=$in_data.",result=[".$result_data."]\n";
file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);

header('HTTP/1.1 200 OK');
?>