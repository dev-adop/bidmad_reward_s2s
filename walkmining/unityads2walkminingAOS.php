<?php

include '../inc/common.php';

header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.

//echo "req hmac".$hash."\n";
//echo "sig hmac".$signature."\n";

$ad_network='UNITYADS';
$trans_id=$_GET['oid'];
$dev_id='';
$app_id=$_GET['app_id'];
$zone_id='';
$amt='';
$currency='';
$verifier= $_GET['hmac'];
$user_id = $_GET['sid'];

$in_file = "/Data/logs/".$ad_network.".log";
$in_data = date("Y-m-d.H:i:s")."-ADOP[".$ad_network."] app_id=".$app_id.",trans_id=".$trans_id.",dev_id=".$dev_id.",amt=".$amt.",verifier=".$verifier.",user_id=".$user_id.",time=".time();

function generate_hash($params, $secret) {
    ksort($params); // All parameters are always checked in alphabetical order
    $s = '';
    foreach ($params as $key => $value) {
        $s .= "$key=$value,";
    }
    $s = substr($s, 0, -1);
    $hash = hash_hmac('md5', $s, $secret);
    return $hash;
}


if($app_id=="3393654")
    $MY_SECRET_KEY="abfe53ecb0587a1b73fb172acc51df41";
elseif($app_id=="3393655")
    $MY_SECRET_KEY="c16628853866cc28618121f95bd47ae1";

$hash = $_GET['hmac'];
unset($_GET['hmac']);
$signature = generate_hash($_GET, $MY_SECRET_KEY); // insert here the secret hash key you received from Unity Ads support

// check signature
if($hash != $signature) {
    header('HTTP/1.1 403 Forbidden');
//    echo "Signature did not match";
    $in_data=$in_data.",result=decline"."\n";
    file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);
    exit;
}

$ADOP_SECRET_KEY="bf24692c-2e69-11ed-a261-0242ac120003";
//verify hash
$sign_string="".$trans_id.$dev_id.$amt.$currency.$ADOP_SECRET_KEY.$user_id;
$sign_result=md5($sign_string);

$get_data = array(
    'ad_network'=>$ad_network,
    'trans_id'=>$trans_id,
    'app_id'=>$app_id,
    'zone_id'=>$zone_id,
    'dev_id'=>$dev_id,
    'amt'=>$amt,
    'currency'=>$currency,
    'verifier'=>$sign_result,
    'user_id'=>$user_id
);

//$result_data = get("https://gnapis.singta.co:13001/adop", $get_data);
$in_data=$in_data.",result=[test]\n";
file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);

// 디버깅용.
//$result_data = post("http://s2s.bidmad.net/mycredit/mycredits2stest.php", $get_data);
//$in_data=$in_data.",result=[".$result_data."]\n";
//file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);

// everything OK, return "1"
header('HTTP/1.1 200 OK');
echo "1";
?>