<?php

include '../inc/common.php';
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.

//echo "req hmac".$hash."\n";
//echo "sig hmac".$signature."\n";

$ad_network='ADMOB';
$trans_id=$_GET['transaction_id'];
$dev_id='';
$app_id=$_GET['app_id'];
$zone_id=$_GET['ad_unit'];
$amt=$_GET['reward_amount'];
$currency=$_GET['reward_item'];
$verifier= $_GET['signature'];
$key_id= $_GET['key_id'];
$user_id = $_GET['user_id'];
$custom_data = $_GET['custom_data'];
$timestamp = $_GET['timestamp'];

$keys =  get("https://www.gstatic.com/admob/reward/verifier-keys.json", "");
$in_file = "/Data/logs/".$ad_network.".log";
$in_data = date("Y-m-d.H:i:s")."-MYCREDIT[".$ad_network."] app_id=".$app_id.",zone_id=".$zone_id.",trans_id=".$trans_id.",dev_id=".$dev_id.",amt=".$amt.",verifier=".$verifier.",user_id=".$user_id.",time=".time().':'.$timestamp.",key_id=".$key_id;
//$in_data = $in_data."\n".$keys;

if(strpos($keys, "\"keyId\":".$key_id.",")==false) {
    header('HTTP/1.1 403 Forbidden');
    echo "Signature did not match";
    $in_data=$in_data.",result=decline"."\n";
    file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);
    exit;
}

$MYCREDIT_SECRET_KEY="1b88659c-1bdf-11ea-978f-2e728ce88125";
//verify hash
$sign_string="".$trans_id.$dev_id.$amt.$currency.$MYCREDIT_SECRET_KEY.$user_id;
$sign_result=md5($sign_string);

$get_data = array(
    'ad_network'=>$ad_network,
    'trans_id'=>$trans_id,
    'app_id'=>$app_id,
    'zone_id'=>$zone_id,
    'dev_id'=>$dev_id,
    'amt'=>$amt,
    'currency'=>$currency,
    'verifier'=>$sign_result,
    'user_id'=>$user_id
);

//https://mobile.mycreditchain.com/adop/callback
$result_data = post("https://mobile-api.mycreditchain.io/adop/callback", $get_data);
$in_data=$in_data.",result=[".$result_data."]\n";
file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);

// 디버깅용.
//$result_data = post("http://s2s.bidmad.net/mycredit/mycredits2stest.php", $get_data);
//$in_data=$in_data.",result=[".$result_data."]\n";
//file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);

// everything OK, return "1"
header('HTTP/1.1 200 OK');
?>