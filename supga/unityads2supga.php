<?php

include '../inc/common.php';

header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.

//echo "req hmac".$hash."\n";
//echo "sig hmac".$signature."\n";

$ad_network='UNITYADS';
$trans_id=$_GET['oid'];
$dev_id='';
$app_id=$_GET['app_id'];
$zone_id='';
$amt='';
$currency='';
$verifier= $_GET['hmac'];
$user_id = $_GET['sid'];

$in_file = "/Data/logs/".$ad_network.".log";
$in_data = date("Y-m-d.H:i:s")."-KREON[".$ad_network."] app_id=".$app_id.",trans_id=".$trans_id.",dev_id=".$dev_id.",amt=".$amt.",verifier=".$verifier.",user_id=".$user_id.",time=".time();

function generate_hash($params, $secret) {
    ksort($params); // All parameters are always checked in alphabetical order
    $s = '';
    foreach ($params as $key => $value) {
        $s .= "$key=$value,";
    }
    $s = substr($s, 0, -1);
    $hash = hash_hmac('md5', $s, $secret);
    return $hash;
}

if($app_id=="3347465")
    $MY_SECRET_KEY="21e307ee44f93dbdc03fb0b33315d2df";
elseif($app_id=="3347456")
    $MY_SECRET_KEY="63dd62ab8ca1db11f57e81bdd3888c80";

$hash = $_GET['hmac'];
unset($_GET['hmac']);
$signature = generate_hash($_GET, $MY_SECRET_KEY); // insert here the secret hash key you received from Unity Ads support

// check signature
if($hash != $signature) {
    header('HTTP/1.1 403 Forbidden');
    echo "Signature did not match";
    $in_data=$in_data.",result=decline"."\n";
    file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);
    exit; }

$KREON_SECRET_KEY="f79e9cda95a9491db0b1bc4b53f53d82";
//verify hash
$sign_string="".$trans_id.$dev_id.$amt.$currency.$KREON_SECRET_KEY.$user_id;
$sign_result=md5($sign_string);

$get_data = array(
    'ad_network'=>$ad_network,
    'trans_id'=>$trans_id,
    'app_id'=>$app_id,
    'zone_id'=>$zone_id,
    'dev_id'=>$dev_id,
    'amt'=>$amt,
    'currency'=>$currency,
    'verifier'=>$sign_result,
    'user_id'=>$user_id
);

// Supga 콜백주소로 변경 해야 함.
get("http://s2s.bidmad.net/kreon/supgas2stest.php", $get_data);

/*if($zone_id=="9842538102") {
    //판다 버블
    //https://testauthsusun.msbservice.net:9081/adop.callback.php
    //get("https://testauthsusun.msbservice.net:9081/adop.callback.php", $get_data);
} elseif($zone_id=="8185346661") {
    //갤럭시 인밴더
    // https://banting.msbservice.net:4930/adop.callback.php
    //get("https://banting.msbservice.net:4930/adop.callback.php", $get_data);
} else {
    $in_data=$in_data."Invalid Zone ID :[".$zone_id."]\n";
    file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);
}*/
$in_data=$in_data.",result=success"."\n";
file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);

// everything OK, return "1"
header('HTTP/1.1 200 OK');
echo "1";
?>