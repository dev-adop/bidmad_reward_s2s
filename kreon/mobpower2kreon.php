<?php
include '../inc/common.php';

header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.

$ad_network='MOBPOWER';
$trans_id=$_POST['order_id'];
$dev_id=$_POST['gaid'];
$app_id=$_POST['app_id'];
$zone_id =$_POST['placement_id'];
$amt=$_POST['amount'];
$currency='';
$verifier=$_POST['sign'];
$user_id = $_POST['user_id'];

$in_file = "/Data/logs/".$ad_network.".log";
$in_data = date("Y-m-d.H:i:s")."-KREON[".$ad_network."] app_id=".$app_id.",trans_id=".$trans_id.",dev_id=".$dev_id.",amt=".$amt.",verifier=".$verifier.",user_id=".$user_id.",time=".time();

function buildSign($param, $token){
    ksort($param);
    $sign = md5(http_build_query($param) . $token);
    return $sign;
}

$MY_SECRET_KEY="e85cf490fc7511e9aaef0800200c9a66";

$hash = $_POST['sign'];
unset($_POST['sign']);
$signature = buildSign($_POST, $MY_SECRET_KEY); // insert here the secret hash key you received from Unity Ads support

// check signature
if($hash != $signature) {
    header('HTTP/1.1 403 Forbidden'); echo "Signature did not match";
    $in_data=$in_data.",result=decline"."\n";
    file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);
    exit; }

$KREON_SECRET_KEY="f79e9cda95a9491db0b1bc4b53f53d82";
//verify hash
$sign_string="".$trans_id.$dev_id.$amt.$currency.$KREON_SECRET_KEY.$user_id;
$sign_result=md5($sign_string);

$get_data = array(
    'ad_network'=>$ad_network,
    'trans_id'=>$trans_id,
    'app_id'=>$app_id,
    'zone_id'=>$zone_id,
    'dev_id'=>$dev_id,
    'amt'=>$amt,
    'currency'=>$currency,
    'verifier'=>$sign_result,
    'user_id'=>$user_id
);
//수순 콜백 주소
//https://testauthsusun.msbservice.net:9081/adop.callback.php
//빅투 콜백 주소
//http://testnewbig2.msbservice.net:3930/adop.callback.php

if($app_id == "1000640") {
    //수순 콜백 주소
    //https://susun.msbservice.net:9081/adop.callback.php
    get("https://susun.msbservice.net:9081/adop.callback.php", $get_data);
}elseif($app_id == "1000639"){
    //빅투 콜백 주소
    // https://banting.msbservice.net:4930/adop.callback.php
    get("https://banting.msbservice.net:4930/adop.callback.php", $get_data);
}else {
    $in_data=$in_data."Invalid Zone ID :[".$zone_id."]\n";
    file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);
}
$in_data=$in_data.",result=success"."\n";
file_put_contents($in_file, $in_data, FILE_APPEND | LOCK_EX);

// everything OK, return "1"
header('HTTP/1.1 200 OK');
echo "{\"status\": 1}";
?>