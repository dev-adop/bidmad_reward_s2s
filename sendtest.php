<?php
// GET 방식 함수
header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header('Pragma: no-cache'); // HTTP 1.0.
header('Expires: 0'); // Proxies.

function get($url, $params=array())
{
    $url = $url.'?'.http_build_query($params, '', '&');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    curl_close($ch);

    return $response;
}
$get_data = array(
    "trans_id"=>"trans_id1",
    "user_id"=>"user_id1"
);
// get함수 호출
get("http://52.78.181.78/kreons2stest.php", $get_data);
//get("http://localhost:8080", $get_data);
echo "1";
?>
